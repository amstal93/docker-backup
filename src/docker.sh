#!/bin/bash

source utils.sh

CONTAINER_BACKUP_STOP_LABEL="backup.stop-before"
CONTAINER_RESTORE_STOP_LABEL="restore.stop-before"
CONTAINER_BACKUP_PRE_SCRIPT_LABEL="backup.pre-script"
CONTAINER_RESTORE_POST_SCRIPT_LABEL="restore.post-script"

function get_running_containers_with_label {
    local label=${1}
    echo $(
        docker ps -a \
            --format "{{.Names}}" \
            --filter "status=running" \
            --filter "label=${label}"
    )
}

function get_exited_containers_with_label {
    local label=${1}
    echo $(
        docker ps -a \
            --format "{{.Names}}" \
            --filter "status=exited" \
            --filter "label=${label}"
    )
}

function stop_labelled_containers {
    local label=${1}
    local containers=$(get_running_containers_with_label ${label})

    if [[ ! -z ${containers} ]]; then
        info "stopping \"${label}\" labelled ${containers} container(s)..."
        docker container stop ${containers} > /dev/null
    else
        info "no \"${label}\" labelled container to stop found..."
    fi
}

function start_labelled_containers {
    local label=${1}
    local containers=$(get_exited_containers_with_label ${label})

    if [[ ! -z ${containers} ]]; then
        info "starting \"${label}\" labelled ${containers} container(s)..."
        docker start ${containers} > /dev/null
    else
        info "no \"${label}\" labelled exited container to start found..."
    fi
}

function get_container_script {
    local container_name=${1}
    local script_label=${2}

    echo $(docker ps -a --filter "name=${container_name}" --format "{{.Label \"${script_label}\"}}")
}

function exec_labelled_containers_scripts {
    local label=${1}

    if [[ ! -z $(get_running_containers_with_label ${label}) ]]; then
        for container_name in $(get_running_containers_with_label ${label})
        do
            script_path=$(get_container_script ${container_name} ${label})
            info "executing \"${script_path}\" script inside \"${label}\" labelled ${container_name} container..."
            docker exec ${container_name} ${script_path}
        done
    else
        info "no \"${label}\" labelled running container found for executing script..."
    fi
}