#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting remove-older-than command..."
eval duplicity remove-older-than "${@}" ${DUPLICITY_REMOVE_OLDER_THAN_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET}
info "done."