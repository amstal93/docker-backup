#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh

info "starting list-current-files command..."
eval duplicity list-current-files ${DUPLICITY_LIST_CURRENT_FILES_ARGS} ${DUPLICITY_COMMON_ARGS} ${TARGET}
info "done."