#!/bin/bash

set -e
[[ ${DEBUG} == true ]] && set -x

source $(dirname $0)/../env.sh
source $(dirname $0)/../utils.sh
source $(dirname $0)/../docker.sh

check_for_empty_source
exec_labelled_containers_scripts ${CONTAINER_BACKUP_PRE_SCRIPT_LABEL}
stop_labelled_containers ${CONTAINER_BACKUP_STOP_LABEL}
info "starting incremental backup command..."
eval duplicity incr ${DUPLICITY_BACKUP_INCR_ARGS} ${DUPLICITY_COMMON_ARGS} ${SOURCE} ${TARGET}
start_labelled_containers ${CONTAINER_BACKUP_STOP_LABEL}
info "done."