FROM alpine:3.8

RUN apk add --no-cache \
    bash \
    openssh \
    duplicity=0.7.17-r1 \
    rsync \
    docker

COPY ./src/*.sh /root/
RUN chmod a+x /root/*.sh

COPY ./src/commands/* /root/commands/
RUN chmod a+x /root/commands/*
RUN for file in /root/commands/*.sh; do mv -- "$file" "${file%%.sh}"; done

ENV PATH=$PATH:/root/commands

VOLUME /root/.cache/duplicity
VOLUME /source
VOLUME /target

WORKDIR /root
ENTRYPOINT [ "/root/entrypoint.sh" ]
CMD [ "periodic-backup" ]